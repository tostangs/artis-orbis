import { Blockchain, Transaction } from 'blockchain.js';
let comcoin = new Blockchain();

comcoin.createTransaction(new Transaction('address1', 'address2', 100));
comcoin.createTransaction(new Transaction('address2', 'address1', 50));

console.log('\n Starting the miner...');
comcoin.minePendingTransactions('comcoin-dev');

console.log('\nBalance of comcoin dev address is ', comcoin.getBalanceOfAddress('comcoin-dev'));

console.log('\n Starting the miner...');
comcoin.minePendingTransactions('comcoin-dev');

console.log('\nBalance of comcoin dev address is ', comcoin.getBalanceOfAddress('comcoin-dev'));